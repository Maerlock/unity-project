﻿using UnityEngine;
using System.Collections;

public abstract class ClickCatcher : MonoBehaviour
{
    public abstract void ClickCatch(RaycastHit hit);
	
	
}
