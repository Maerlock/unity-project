﻿using UnityEngine;
using System.Collections;
using System;

public class Clickaction : MonoBehaviour
{
    [SerializeField]
    private GameObject Cubeprefab;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
       {
            Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rhInfo;
            bool didHit = Physics.Raycast(toMouse, out rhInfo);
            if (didHit)
            {
                Debug.Log(rhInfo.collider.name + " ... " + rhInfo.point);
                Destroyable destScipt = rhInfo.collider.GetComponent<Destroyable>();
                if (destScipt != null)
                {
                    destScipt.RemoveMe();

                }
                if (rhInfo.collider.tag == "Ground")
                {
                    Debug.Log("Cube prefab has been created");
                  Vector3 initialPosition = new Vector3();
                   GameObject newCube = Instantiate(Cubeprefab, initialPosition, Quaternion.identity) as GameObject;
                    newCube.transform.SetParent(transform);
                    newCube.transform.position = rhInfo.point;
                } 
                
                
                  
             }
            
        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rhInfo;
            bool didHit = Physics.Raycast(toMouse, out rhInfo);
            if (didHit)
            {
                Debug.Log(rhInfo.collider.name + " ... " + rhInfo.point);
                Recolorable recolorScipt = rhInfo.collider.GetComponent<Recolorable>();
                if (recolorScipt != null)
                {
                    recolorScipt.RecolorMeOnClick();

                }
            }
        }
    }

}





