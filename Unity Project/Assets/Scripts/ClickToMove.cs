﻿using UnityEngine;
using System.Collections;


public class ClickToMove : MonoBehaviour
{

    [SerializeField]
    [Range(1, 20)]
    private float speed = 10;

    private Vector3 targetPosition;
    private bool isMoving;
    private bool isJumping;
    private Animator anim;
    [SerializeField]
    private bool grounded;
    [SerializeField]
    private Transform GroundCheck;
    [SerializeField]
    private LayerMask WhatIsGround;
    const int LEFT_MOUSE_BUTTON = 0;
    Rigidbody rig;
    [SerializeField]
    private AnimationCurve SlowMotion;
    [SerializeField]
    private bool moveIgnor;

    void Start()
    {
        targetPosition = transform.position;
        isMoving = false;
        anim = GetComponent<Animator>();
        rig = GetComponent<Rigidbody>();
        grounded = true;
        moveIgnor = false;
    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0) && Input.GetMouseButtonDown(1))
            {
            moveIgnor = true;
        }

        if (Input.GetMouseButtonUp(0) && Input.GetMouseButtonUp(1))
        {
            moveIgnor = false;
        }

        if (moveIgnor == false)
        {

            if (Input.GetMouseButtonDown(1) && grounded)
            {
                Debug.Log("Jump");
                rig.AddForce(new Vector3(0, 350f, 0));
                anim.SetBool("Jumping", true);
            }


            if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
                SetTargetPosition();
            {
                if (isMoving)
                    MovePlayer();
                if (isMoving)
                    anim.SetBool("Walking", true);
                else
                    anim.SetBool("Walking", false);
            }
        }
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Ground"))
        {
            grounded = true;
            anim.SetBool("Jumping", false);
        }
    }


    void SetTargetPosition()
    {
        Plane plane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float point = 0f;

        if (plane.Raycast(ray, out point))
            targetPosition = ray.GetPoint(point);

        isMoving = true;
    }

    void MovePlayer()
    {
        transform.LookAt(targetPosition);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        if (transform.position == targetPosition)
            isMoving = false;

        Debug.DrawLine(transform.position, targetPosition, Color.red);
    }
}