﻿using UnityEngine;
using System.Collections;

public class MainCyclopAI : MonoBehaviour
{
    [System.Serializable]

    public class Axe
    {
        public int Damage;
        public float Distance;
        public bool Armor;
    }

    public Axe[] Mace;

    [System.Serializable]
    public class Ammo
    {
        public GameObject Bullet;
        public int Damage;
        public float Distance;
        public bool Armor;
        public int Speed;
    }
    public Ammo Shells;

    /// /////////////////////////////////////////////////////////

    [System.Serializable]
    public class AnimationsCyclopAI
    {
        public AnimationClip Idle;
        public AnimationClip Walk;
        public AnimationClip Run;
        public AnimationClip Death;
        public AnimationClip Attack_1;
        public AnimationClip Attack_2;

    }
    public AnimationsCyclopAI Anim_Cyclop;

    private Animator animation_switcher;
    private int RNGAttack_1;
    private float RNGAttack_2;
    [SerializeField]
    public GameObject Point_1;
    [SerializeField]
    public GameObject Point_2;
    [SerializeField]
    public GameObject Point_3;
    [SerializeField]
    public GameObject Point_4;
    public bool Agro;
    RaycastHit RCH_1;
    [SerializeField]
    private GameObject target;

    void Start()
    {
        animation_switcher = GetComponent<Animator>();
        Agro = false;
        


    }

    // Update is called once per frame
    //void Update()
    //{



    //    if (Agro == false)
    //    {

    //        if (Vector3.Distance(transform.position, Point_1.transform.position) <= 1f)
    //        {

    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Point_2.transform.position - transform.position), 10 * time.deltatime);
    //            animation_switcher.SetInteger("SwitchAnim", 1);

    //        }

    //        if (Vector3.Distance(transform.position, Point_2.transform.position) <= 1f)
    //        {
    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Point_3.transform.position - transform.position), 10 * time.deltatime);
    //            animation_switcher.SetInteger("SwitchAnim", 1);

    //        }

    //        if (Vector3.Distance(transform.position, Point_3.transform.position) <= 1f)
    //        {
    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Point_4.transform.position - transform.position), 10 * time.deltatime);
    //            animation_switcher.SetInteger("SwitchAnim", 1);

    //        }

    //        if (Vector3.Distance(transform.position, Point_4.transform.position) <= 1f)
    //        {
    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(Point_1.transform.position - transform.position), 10 * time.deltatime);
    //            animation_switcher.SetInteger("SwitchAnim", 1);
    //        }

    //        if (physics.raycast(transform.position, transform.forward, out rch_1, 1000.0f))
    //        {
    //            if (rch_1.collider.tag == "Point")
    //                transform.position += transform.forward * 3 * time.deltatime;
    //        }


    //        transform.position += transform.forward * 3 * time.deltatime;

    //    }

    //    else
    //    {
    //        if (Vector3.Distance(transform.position, Gameobject.findGameobjectwithtag("player").transform.position) <= 3)
    //        {
    //            animation_switcher.SetInteger("SwitchAnim", 2);
    //            rngattack_2 += time.deltatime;
    //            if (rngattack_2 >= 1.5f)
    //            {
    //                rngattack_1 = random.range(0, 2);
    //                rngattack_2 = 0;
    //            }
    //            if (rngattack_1 == 0)
    //                getcomponent<animation>().play(anim_cyclop.attack_1.name);
    //            if (rngattack_1 == 1)
    //                getcomponent<animation>().play(anim_cyclop.attack_2.name);

    //        }

    //        else
    //        {
    //            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(position_between_player), 60 * time.deltatime);
    //            animation_switcher = 2;
    //            transform.position += transform.forward * 6 * time.deltatime;


    //        }
    //    }
    //    if (Vector3.Distance(transform.position, Gameobject.findGameobjectwithtag("player").transform.position) <= 4)
    //    {
    //        Agro = true;
    //    }

    //}
}
