﻿using UnityEngine;
using System.Collections;

public class CylinderClickCatcher : ClickCatcher
{
    public override void ClickCatch(RaycastHit hit)
    {
        GetComponent<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);
    }

}
