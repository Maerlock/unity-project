﻿using UnityEngine;
using System.Collections;

public class AimCreate : MonoBehaviour
{
    [SerializeField]
    private GameObject aimPrefab;
    internal GameObject aimer;

    void Start()
    {
        CreateAim();
    }
    public void CreateAim()
    {
        Vector3 AimPosition = new Vector3(0, 0, 0);
        aimer = Instantiate(aimPrefab, AimPosition, Quaternion.LookRotation(Vector3.down)) as GameObject;
    }
}
