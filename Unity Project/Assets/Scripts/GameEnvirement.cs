﻿using UnityEngine;
using System.Collections;

public class GameEnvirement : MonoBehaviour
{
    [SerializeField]
    private GameObject bubblePrefab;
    [SerializeField]
    private float minX;
    [SerializeField]
    private float maxX;
    [SerializeField]
    private float minZ;
    [SerializeField]
    private float maxZ;
    [SerializeField]
    private float y;
    [SerializeField]
    private Camera Camera;
    [SerializeField]
    private GameObject AimPrefab;
    public GameObject Aim;
    void Update()
    {
        RaycastHit hit;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("Ground"))
                {
                    Aim.GetComponent<AimBehaviour>().AimOn();
                    Aim.transform.position = hit.point;

                }
            }
        }
    }
    //private void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        var cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
    //       RaycastHit hit;
    //        if (Physics.Raycast(cameraRay, out hit))
    //        {
    //            var clickCatcher = hit.transform.gameObject.GetComponent<ClickCatcher>();


    //        if (clickCatcher != null)
    //            {
    //                clickCatcher.ClickCatch(hit);
    //            }
    //        }
    //    }
    //}

    private void Start()
    {
        GetComponent<timer>().OnTimeEllapsed += BubbleTimeToAppear;
        CreateAim();
    }

    private void CreateOneRainBubble()
    {
        Vector3 initialPosition = new Vector3(UnityEngine.Random.Range(minX, maxX), y, UnityEngine.Random.Range(minZ, maxZ));
        GameObject newBubble = Instantiate(bubblePrefab, initialPosition, Quaternion.identity) as GameObject;
        newBubble.transform.SetParent(transform);
        newBubble.GetComponent<Bubble>().OnFallToAbyss += BubbleFallToAbyss;
    }
    private void BubbleFallToAbyss(object sender, Bubble.FallToAbyssEventArgs args)
    {
        args.FalledBubble.OnFallToAbyss -= BubbleFallToAbyss;

        Destroy(args.FalledBubble.gameObject);
    }
    private void BubbleTimeToAppear(object sender, timer.TimeToAppearEventArgs args)
    {
        CreateOneRainBubble();
    }

    private void OnDestroy()
    {
        GetComponent<timer>().OnTimeEllapsed -= BubbleTimeToAppear;
    }
    private void CreateAim()
    {
        Vector3 AimPosition = new Vector3(0, +1, 0);
        Aim = Instantiate(AimPrefab, AimPosition, Quaternion.LookRotation(Vector3.down)) as GameObject;
        Aim.SetActive(false);
    }
}


        
      
 


