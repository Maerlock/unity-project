﻿using UnityEngine;
using System.Collections;

public class Recolorable : MonoBehaviour {

    public void RecolorMeOnClick()
    {
        Debug.Log("Recolorable's RecolorMeOnClick function is being called on" + name);
        gameObject.GetComponent<Renderer> ().material.color = Color.blue;

    }
}
