﻿using UnityEngine;
using System.Collections;
using System;

public class timer : MonoBehaviour

  {
public event EventHandler<TimeToAppearEventArgs> OnTimeEllapsed;
[SerializeField]
private float timerTime;
private float timerStart;
private float timeLeft;

	void Start()
    {
        timerStart = Time.time;
           }


	void Update()
    {
        timeLeft = Time.time - timerStart;
               if (timerTime <= timeLeft)
                   {
            timerStart = Time.time;
            
                       if (OnTimeEllapsed != null)
                           {
                OnTimeEllapsed(this, new TimeToAppearEventArgs(this));
                           }
                   }
           }
	public class TimeToAppearEventArgs : EventArgs
	{
		public timer AppearBubble { get; private set; }
		public TimeToAppearEventArgs(timer OnTimeEllapsed)
		{
            AppearBubble = OnTimeEllapsed;
		}
	}
}