﻿using UnityEngine;
using System.Collections;
using System;

public class GroundClickCatcher : ClickCatcher
{
    public override void ClickCatch(RaycastHit hit)
    {
        var go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        go.GetComponent<Renderer>().material.color = Color.green;
        go.AddComponent<Rigidbody>();
        go.GetComponent<CylinderClickCatcher>();
    }
}
    
