﻿using UnityEngine;
using System.Collections;

public class AimBehaviour : MonoBehaviour
{
    [SerializeField]
    private AnimationCurve aimCurve;
    [SerializeField]
    private float Endtime = 0, StartTime;
    private Renderer aimRenderer;
    private Color color;


    public void AimOn()
    {
        gameObject.SetActive(true);
        StartTime = Time.time;
    }

    public void Start()
    {
        StartTime = Time.time;
        aimRenderer = GetComponent<Renderer>();
        color = aimRenderer.material.color;
        color.a = 1.0f;
    }

    public void Update()
    {
        if (gameObject.activeSelf == true)
        {
            Endtime = Time.time - StartTime;
            color.a = aimCurve.Evaluate(Endtime);
            aimRenderer.material.color = color;
            if (Endtime >= 2.0f)
            {
                color.a = 1.0f;
                Endtime = 0;
                gameObject.SetActive(false);
                StartTime = Time.time;
            }

        }
    }
}