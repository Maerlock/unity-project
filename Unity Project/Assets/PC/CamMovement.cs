﻿using UnityEngine;
using System.Collections;

public class CamMovement : MonoBehaviour
{
	[SerializeField]
    private GameObject Target;
	[SerializeField]
    private float Speed, SpeedScroll, MinDis, MaxDis;

	private bool _isActive = false;
    private float _x, _y, DistanceForTarget;

public void Update()
    {


		_x = Input.GetAxis("Mouse X")*Speed*10;
		_y = Input.GetAxis("Mouse Y")*-Speed*10;


        if (Input.GetMouseButtonDown(0) && Input.GetMouseButtonDown(1))
        {
            _isActive = true;

        }

        if (Input.GetMouseButtonUp(1) || Input.GetMouseButtonUp(0))
        {
            _isActive = false;
        }

        if (_isActive)
		{
			transform.RotateAround(Target.transform.position, transform.up, _x* Time.deltaTime);
  			transform.RotateAround(Target.transform.position, transform.right, _y* Time.deltaTime);

			transform.rotation = Quaternion.LookRotation(Target.transform.position - transform.position);
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
		}
		if(Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			DistanceForTarget = Vector3.Distance(transform.position, Target.transform.position);
			if(Input.GetAxis("Mouse ScrollWheel") > 0 && DistanceForTarget >MinDis)
			{
				transform.Translate(Vector3.forward* Time.deltaTime * SpeedScroll);
			}

			if(Input.GetAxis("Mouse ScrollWheel") < 0 && DistanceForTarget <MaxDis)
			{
				transform.Translate(Vector3.forward* Time.deltaTime * -SpeedScroll);	
			}
		}
	}
}
