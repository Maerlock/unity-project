﻿using UnityEngine;
using System.Collections;
using System;

public class Envirement : MonoBehaviour
{
    private GameObject Soil;
    private GameObject Ground;
    private GameObject Grass;

    [SerializeField]
    private GameObject GroundPrefab1;

    [SerializeField]
    private GameObject SoilPrefab2;

    [SerializeField]
    private GameObject GrassPrefab3;

    public void Update()
    {
        if (Input.GetKeyDown("1"))
        {

            RaycastHit RayHit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RayHit))
            {

                if (RayHit.collider.CompareTag("Ground"))
                {

                    Vector3 Position = RayHit.collider.transform.position;
                    Position += RayHit.normal;
                    Ground = Instantiate(GroundPrefab1, Position, Quaternion.identity) as GameObject;
                    Ground.transform.parent = transform;
                }

            }
            else
            {
                if (RayHit.collider.CompareTag("Soil"))
                {
                    Vector3 Position = RayHit.collider.transform.position;
                    Position += RayHit.normal;
                    Ground = Instantiate(GroundPrefab1, Position, Quaternion.identity) as GameObject;
                    Ground.transform.parent = transform;
                }




            }
        }

        if (Input.GetKeyDown("2"))
        {

            RaycastHit RayHit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RayHit))
            {

                if (RayHit.collider.CompareTag("Ground"))
                {
                    Vector3 Position = RayHit.collider.transform.position;
                    Position += RayHit.normal;
                    Soil = Instantiate(SoilPrefab2, Position, Quaternion.identity) as GameObject;
                    Soil.transform.parent = transform;

                }
                else

                {

                    if (RayHit.collider.CompareTag("Soil"))
                    {
                        Vector3 Position = RayHit.collider.transform.position;
                        Position += RayHit.normal;
                        Soil = Instantiate(SoilPrefab2, Position, Quaternion.identity) as GameObject;
                        Soil.transform.parent = transform;
                    }
                    else



                    {

                        if (RayHit.collider.CompareTag("Grass"))
                        {
                            Vector3 Position = RayHit.collider.transform.position;
                            Position += RayHit.normal;
                            Soil = Instantiate(SoilPrefab2, Position, Quaternion.identity) as GameObject;
                            Soil.transform.parent = transform;
                        }

                    }
                }
            }
        }
        if (Input.GetKeyDown("3"))
        {
            RaycastHit RayHit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RayHit))
            {
                if (RayHit.collider.CompareTag("Soil"))
                {

                    Vector3 Position = RayHit.collider.transform.position;
                    Position += RayHit.normal;
                    Grass = Instantiate(GrassPrefab3, Position, Quaternion.identity) as GameObject;
                    Grass.transform.parent = transform;
                }

            }



        }
        if (Input.GetKeyDown("0"))
        {
            Ground = Instantiate(GroundPrefab1, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
        }
    }
}
        
        
      
 

