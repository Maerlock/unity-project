﻿using UnityEngine;
using System.Collections;

public class GameEnvirement : MonoBehaviour
{
    public GameObject sphere;
	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Inst()); //Старт сопрограммы
    }
	
    IEnumerator Inst() //Интерфейс предоставляет методы, позволяющие получать содержимое коллекции по очереди
    {
        yield return new WaitForSeconds(0.1f); // Продолжить через некоторое время
        Vector3 gameObject = new Vector3(Random.Range(-5.0F, 5.0F), 5, Random.Range(-5.0F, 5.0F)); // координаты объекта X, Y, Z
        GameObject ob = Instantiate(sphere,gameObject,Quaternion.identity) as GameObject; // создание объекта
        Destroy(ob, 1); //уничтожение чего и через сколько сек.
        Repeat();
    }
    void Repeat()
    {
        StartCoroutine(Inst());

    }
}
